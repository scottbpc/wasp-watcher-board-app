long previous = 0;
int patient_id = 999;
long long last_movement_millis = 0;
long long twelve_hours_millis = 43200000;
char * panic_sms_number = "0861111111";

char panic_message[128];

void setup()
{
  // Setup for GPRS to send data to server
  GPRS.ON();
  USB.println("GPRS module ready...");
  while(!GPRS.check());
  USB.println("GPRS connected to the network");
  
  // configure SMS and Incoming Calls
  if(GPRS.setInfoIncomingCall()) USB.println("Info Incoming Call OK");
  if(GPRS.setInfoIncomingSMS()) USB.println("Info Incoming SMS OK");
  if(GPRS.setTextModeSMS()) USB.println("Text Mode SMS OK");
  // Set up the SD card for writing door movement data out to
  SD.ON();
  
  // Set up accelerometer to detect door movements
  ACC.ON();
  ACC.setFF();
  USB.begin(); // starts using the serial port
  
  sprintf(panic_message,"WARNING! No door movement for patient %d in last 12 hours - please investigate.", patient_id);
}

void loop()
{
  // check the interruptions for 2 seconds
  previous = millis();
  while(millis() - previous <= 2000)
  {
    if(intFlag & ACC_INT) {
      detectDoorMovement();
    }
  }
  // If there was no movement in the last 12 hours
  if (last_movement_millis + twelve_hours_millis > millis()) {
    sendPanicSms();
  }
}

void sendPanicSms() {
  if(GPRS.sendSMS(panic_message, panic_sms_number)); 
}

char* SERVER_ADDRESS = "http://madamanu.com/";

void sendMessageToServer(long x, long z) {
  // Configure GPRS Connection
  int i = 0;
  if(GPRS.configureGPRS()) USB.println("Configured OK");
  if(GPRS.createSocket(SERVER_ADDRESS,"80",GPRS_CLIENT)){
    USB.println("Socket Opened OK");
    USB.print("Session Number: ");
    while( GPRS.socket_ID[i]!='\r' ){
      USB.print(GPRS.socket_ID[i]-'0',DEC);
      i++;
    }
    i=0;
    USB.println();
  }
  else USB.println("Error opening the socket");
  delay(500);
  
  char message[64];
  // Format a HTTP POST request to tell the server to log movement.
  // We want to POST something like: uid=123&x=345&z=566
  sprintf(message,"POST /upload HTTP/1.1\r\nuid=%lld&x=%d&y=d", patient_id, x, z);
  
  GPRS.sendData(message,GPRS.socket_ID);
  if(GPRS.closeSocket(GPRS.socket_ID)) USB.println("Socket closed");
}

void detectDoorMovement() {
  // clear the accelerometer interrupt flag on the general interrupt vector
  intFlag &= ~(ACC_INT);

  // read the acceleration source register
  int auxReg = ACC.readRegister(FF_WU_SRC);

  USB.println(auxReg,DEC);
  
  // Only print results if the interrupt bit is active.
  if (auxReg & IA)
  {
    // Only log data if there's movement on the X or Z axis - this means that the door was moved
    if ((auxReg & XHIE) || (auxReg & ZHIE)) {
      USB.print("\nDetected door open/close movement! ");
      // Uploads data to the webapp to be stored in the logging database
      sendMessageToServer(ACC.getX(), ACC.getZ());
      // Update the last movement timestamp (timestamp since startup)
      last_movement_millis = millis();
    }
    
    Utils.blinkLEDs(1000);
    // debounce the connection - don't want this to count as several movements
    delay(2000);
  }
}
